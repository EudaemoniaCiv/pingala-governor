--==========================
--******	 PINGALA	  ******
--==========================

	-- Clear all promotions so we can start again.
	DELETE FROM GovernorPromotionSets WHERE GovernorType = 'GOVERNOR_THE_EDUCATOR';
	DELETE FROM GovernorPromotions WHERE GovernorPromotionType = 'GOVERNOR_PROMOTION_EDUCATOR_LIBRARIAN';
	DELETE FROM GovernorPromotions WHERE GovernorPromotionType = 'GOVERNOR_PROMOTION_EDUCATOR_CONNOISSEUR';
	DELETE FROM GovernorPromotions WHERE GovernorPromotionType = 'GOVERNOR_PROMOTION_EDUCATOR_RESEARCHER';
	DELETE FROM GovernorPromotions WHERE GovernorPromotionType = 'GOVERNOR_PROMOTION_EDUCATOR_GRANTS';
	DELETE FROM GovernorPromotions WHERE GovernorPromotionType = 'GOVERNOR_PROMOTION_MERCHANT_CURATOR';
	DELETE FROM GovernorPromotions WHERE GovernorPromotionType = 'GOVERNOR_PROMOTION_EDUCATOR_SPACE_INITIATIVE';

	-- NEW PROMOTIONS
	INSERT INTO Types (Type, Kind)
		VALUES
		('GOVERNOR_PROMOTION_EUD_EDUCATOR_SCHOLASTIC_DOCTRINE', 'KIND_GOVERNOR_PROMOTION'),
		('GOVERNOR_PROMOTION_EUD_EDUCATOR_RESEARCHER', 'KIND_GOVERNOR_PROMOTION'),
		('GOVERNOR_PROMOTION_EUD_EDUCATOR_ROYAL_ARCHIVES', 'KIND_GOVERNOR_PROMOTION'),
		('GOVERNOR_PROMOTION_EUD_EDUCATOR_GRANTS', 'KIND_GOVERNOR_PROMOTION'),
		('GOVERNOR_PROMOTION_EUD_EDUCATOR_NATIONAL_COLLEGE', 'KIND_GOVERNOR_PROMOTION'),
		('GOVERNOR_PROMOTION_EUD_EDUCATOR_CURATOR', 'KIND_GOVERNOR_PROMOTION');

	INSERT INTO GovernorPromotionSets (GovernorType, GovernorPromotion)
		VALUES
		('GOVERNOR_THE_EDUCATOR', 'GOVERNOR_PROMOTION_EUD_EDUCATOR_SCHOLASTIC_DOCTRINE'),
		('GOVERNOR_THE_EDUCATOR', 'GOVERNOR_PROMOTION_EUD_EDUCATOR_RESEARCHER'),
		('GOVERNOR_THE_EDUCATOR', 'GOVERNOR_PROMOTION_EUD_EDUCATOR_ROYAL_ARCHIVES'),
		('GOVERNOR_THE_EDUCATOR', 'GOVERNOR_PROMOTION_EUD_EDUCATOR_GRANTS'),
		('GOVERNOR_THE_EDUCATOR', 'GOVERNOR_PROMOTION_EUD_EDUCATOR_NATIONAL_COLLEGE'),
		('GOVERNOR_THE_EDUCATOR', 'GOVERNOR_PROMOTION_EUD_EDUCATOR_CURATOR');

	INSERT INTO GovernorPromotions (GovernorPromotionType, Name, Description, Level, "Column", BaseAbility)
		VALUES
		('GOVERNOR_PROMOTION_EUD_EDUCATOR_SCHOLASTIC_DOCTRINE', 'LOC_GOVERNOR_PROMOTION_EUD_EDUCATOR_SCHOLASTIC_DOCTRINE_NAME', 'LOC_GOVERNOR_PROMOTION_EUD_EDUCATOR_SCHOLASTIC_DOCTRINE_DESCRIPTION', '0', '1', 1),
		('GOVERNOR_PROMOTION_EUD_EDUCATOR_RESEARCHER', 'LOC_GOVERNOR_PROMOTION_EUD_EDUCATOR_RESEARCHER_NAME', 'LOC_GOVERNOR_PROMOTION_EUD_EDUCATOR_RESEARCHER_DESCRIPTION', '1', '0', 0),
		('GOVERNOR_PROMOTION_EUD_EDUCATOR_ROYAL_ARCHIVES', 'LOC_GOVERNOR_PROMOTION_EUD_EDUCATOR_ROYAL_ARCHIVES_NAME', 'LOC_GOVERNOR_PROMOTION_EUD_EDUCATOR_ROYAL_ARCHIVES_DESCRIPTION', '1', '2', 0),
		('GOVERNOR_PROMOTION_EUD_EDUCATOR_GRANTS', 'LOC_GOVERNOR_PROMOTION_EUD_EDUCATOR_GRANTS_NAME', 'LOC_GOVERNOR_PROMOTION_EUD_EDUCATOR_GRANTS_DESCRIPTION', '2', '1', 0),
		('GOVERNOR_PROMOTION_EUD_EDUCATOR_NATIONAL_COLLEGE', 'LOC_GOVERNOR_PROMOTION_EUD_EDUCATOR_NATIONAL_COLLEGE_NAME', 'LOC_GOVERNOR_PROMOTION_EUD_EDUCATOR_NATIONAL_COLLEGE_DESCRIPTION', '3', '0', 0),
		('GOVERNOR_PROMOTION_EUD_EDUCATOR_CURATOR', 'LOC_GOVERNOR_PROMOTION_EUD_EDUCATOR_CURATOR_NAME', 'LOC_GOVERNOR_PROMOTION_EUD_EDUCATOR_CURATOR_DESCRIPTION', '3', '2', 0);

	INSERT INTO GovernorPromotionPrereqs (GovernorPromotionType, PrereqGovernorPromotion)
		VALUES
		('GOVERNOR_PROMOTION_EUD_EDUCATOR_RESEARCHER', 'GOVERNOR_PROMOTION_EUD_EDUCATOR_SCHOLASTIC_DOCTRINE'),
		('GOVERNOR_PROMOTION_EUD_EDUCATOR_ROYAL_ARCHIVES', 'GOVERNOR_PROMOTION_EUD_EDUCATOR_SCHOLASTIC_DOCTRINE'),
		('GOVERNOR_PROMOTION_EUD_EDUCATOR_GRANTS', 'GOVERNOR_PROMOTION_EUD_EDUCATOR_RESEARCHER'),
		('GOVERNOR_PROMOTION_EUD_EDUCATOR_GRANTS', 'GOVERNOR_PROMOTION_EUD_EDUCATOR_ROYAL_ARCHIVES'),
		('GOVERNOR_PROMOTION_EUD_EDUCATOR_NATIONAL_COLLEGE', 'GOVERNOR_PROMOTION_EUD_EDUCATOR_GRANTS'),
		('GOVERNOR_PROMOTION_EUD_EDUCATOR_CURATOR', 'GOVERNOR_PROMOTION_EUD_EDUCATOR_GRANTS');

	INSERT INTO GovernorPromotionModifiers (GovernorPromotionType, ModifierId)
		VALUES
		('GOVERNOR_PROMOTION_EUD_EDUCATOR_SCHOLASTIC_DOCTRINE','EUD_EDUCATOR_SCHOLASTIC_DOCTRINE_CAMPUS_DISTRICT_PRODUCTION'),
		('GOVERNOR_PROMOTION_EUD_EDUCATOR_SCHOLASTIC_DOCTRINE','EUD_EDUCATOR_SCHOLASTIC_DOCTRINE_CAMPUS_BUILDING_PRODUCTION'),
		('GOVERNOR_PROMOTION_EUD_EDUCATOR_SCHOLASTIC_DOCTRINE','EUD_EDUCATOR_SCHOLASTIC_DOCTRINE_THEATER_SQUARE_DISTRICT_PRODUCTION'),
		('GOVERNOR_PROMOTION_EUD_EDUCATOR_SCHOLASTIC_DOCTRINE','EUD_EDUCATOR_SCHOLASTIC_DOCTRINE_THEATER_SQUARE_BUILDING_PRODUCTION'),
		('GOVERNOR_PROMOTION_EUD_EDUCATOR_RESEARCHER','RESEARCHER_SCIENCE_CITIZEN'),
		('GOVERNOR_PROMOTION_EUD_EDUCATOR_ROYAL_ARCHIVES','EUD_EDUCATOR_ROYAL_ARCHIVES_WRITING_SCIENCE'),
		('GOVERNOR_PROMOTION_EUD_EDUCATOR_ROYAL_ARCHIVES','EUD_EDUCATOR_ROYAL_ARCHIVES_RELIC_SCIENCE'),
		('GOVERNOR_PROMOTION_EUD_EDUCATOR_ROYAL_ARCHIVES','EUD_EDUCATOR_ROYAL_ARCHIVES_ARTIFACT_SCIENCE'),
		('GOVERNOR_PROMOTION_EUD_EDUCATOR_GRANTS','EDUCATOR_INCREASE_CITY_GREAT_PERSON_POINT_BONUS'),
		('GOVERNOR_PROMOTION_EUD_EDUCATOR_NATIONAL_COLLEGE','EUD_NATIONAL_COLLEGE_SCIENCE_ATTACH_REGIONAL_MODIFIER'),

	INSERT INTO Modifiers (ModifierId, ModifierType)
		VALUES
		('EUD_EDUCATOR_SCHOLASTIC_DOCTRINE_CAMPUS_DISTRICT_PRODUCTION', 'MODIFIER_EUD_SINGLE_CITY_ADJUST_DISTRICT_PRODUCTION'),
		('EUD_EDUCATOR_SCHOLASTIC_DOCTRINE_CAMPUS_BUILDING_PRODUCTION', 'MODIFIER_SINGLE_CITY_ADJUST_BUILDING_PRODUCTION'),
		('EUD_EDUCATOR_SCHOLASTIC_DOCTRINE_THEATER_SQUARE_DISTRICT_PRODUCTION', 'MODIFIER_EUD_SINGLE_CITY_ADJUST_DISTRICT_PRODUCTION'),
		('EUD_EDUCATOR_SCHOLASTIC_DOCTRINE_THEATER_SQUARE_BUILDING_PRODUCTION', 'MODIFIER_SINGLE_CITY_ADJUST_BUILDING_PRODUCTION'),
		('EUD_EDUCATOR_ROYAL_ARCHIVES_WRITING_SCIENCE', 'MODIFIER_SINGLE_CITY_ADJUST_GREATWORK_YIELD'),
		('EUD_EDUCATOR_ROYAL_ARCHIVES_RELIC_SCIENCE', 'MODIFIER_SINGLE_CITY_ADJUST_GREATWORK_YIELD'),
		('EUD_EDUCATOR_ROYAL_ARCHIVES_ARTIFACT_SCIENCE', 'MODIFIER_SINGLE_CITY_ADJUST_GREATWORK_YIELD'),


	INSERT INTO ModifierArguments (ModifierId, Name, Value)
		VALUES
		('EUD_EDUCATOR_SCHOLASTIC_DOCTRINE_CAMPUS_DISTRICT_PRODUCTION', 'DistrictType', 'DISTRICT_CAMPUS'),
		('EUD_EDUCATOR_SCHOLASTIC_DOCTRINE_CAMPUS_DISTRICT_PRODUCTION', 'Amount', '20'),
		('EUD_EDUCATOR_SCHOLASTIC_DOCTRINE_CAMPUS_BUILDING_PRODUCTION', 'DistrictType', 'DISTRICT_CAMPUS'),
		('EUD_EDUCATOR_SCHOLASTIC_DOCTRINE_CAMPUS_BUILDING_PRODUCTION', 'Amount', '20'),
		('EUD_EDUCATOR_SCHOLASTIC_DOCTRINE_THEATER_SQUARE_DISTRICT_PRODUCTION', 'DistrictType', 'DISTRICT_THEATER_SQUARE'),
		('EUD_EDUCATOR_SCHOLASTIC_DOCTRINE_THEATER_SQUARE_DISTRICT_PRODUCTION', 'Amount', '20'),
		('EUD_EDUCATOR_SCHOLASTIC_DOCTRINE_THEATER_SQUARE_BUILDING_PRODUCTION', 'DistrictType', 'DISTRICT_THEATER_SQUARE'),
		('EUD_EDUCATOR_SCHOLASTIC_DOCTRINE_THEATER_SQUARE_BUILDING_PRODUCTION', 'Amount', '20'),
		('EUD_EDUCATOR_ROYAL_ARCHIVES_WRITING_SCIENCE', 'GreatWorkObjectType', 'GREATWORKOBJECT_WRITING'),
		('EUD_EDUCATOR_ROYAL_ARCHIVES_WRITING_SCIENCE', 'YieldType', 'YIELD_SCIENCE'),
		('EUD_EDUCATOR_ROYAL_ARCHIVES_WRITING_SCIENCE', 'Amount', '2'),
		('EUD_EDUCATOR_ROYAL_ARCHIVES_RELIC_SCIENCE', 'GreatWorkObjectType', 'GREATWORKOBJECT_RELIC'),
		('EUD_EDUCATOR_ROYAL_ARCHIVES_RELIC_SCIENCE', 'YieldType', 'YIELD_SCIENCE'),
		('EUD_EDUCATOR_ROYAL_ARCHIVES_RELIC_SCIENCE', 'Amount', '2'),
		('EUD_EDUCATOR_ROYAL_ARCHIVES_ARTIFACT_SCIENCE', 'GreatWorkObjectType', 'GREATWORKOBJECT_ARTIFACT'),
		('EUD_EDUCATOR_ROYAL_ARCHIVES_ARTIFACT_SCIENCE', 'YieldType', 'YIELD_SCIENCE'),
		('EUD_EDUCATOR_ROYAL_ARCHIVES_ARTIFACT_SCIENCE', 'Amount', '2'),

	-- Separate Modifier Set for National College

	-- RequirementSet and Requirement for Plot has Campus within 6.
	INSERT INTO RequirementSets (RequirementSetId , RequirementSetType)
		VALUES 
		('EUD_NATIONAL_COLLEGE_SCIENCE_ATTACH_REGIONAL_MODIFIER_REQUIREMENTS' , 'REQUIREMENTSET_TEST_ANY');
	INSERT INTO RequirementSetRequirements (RequirementSetId , RequirementId)
		VALUES 
		('EUD_NATIONAL_COLLEGE_SCIENCE_ATTACH_REGIONAL_MODIFIER_REQUIREMENTS' , 'EUD_REQUIRES_PLOT_HAS_CAMPUS_WITHIN_6');
	INSERT INTO Requirements (RequirementId , RequirementType)
		VALUES 
		('EUD_REQUIRES_PLOT_HAS_CAMPUS_WITHIN_6' , 'REQUIREMENT_PLOT_ADJACENT_DISTRICT_TYPE_MATCHES');
	INSERT INTO RequirementArguments (RequirementId , Name , Value)
		VALUES 
		('EUD_REQUIRES_PLOT_HAS_CAMPUS_WITHIN_6' , 'DistrictType' ,'DISTRICT_CAMPUS');
	INSERT INTO RequirementArguments (RequirementId , Name , Value)
		VALUES 
		('EUD_REQUIRES_PLOT_HAS_CAMPUS_WITHIN_6' , 'MaxRange' ,'6');
	INSERT INTO RequirementArguments (RequirementId , Name , Value)
		VALUES 
		('EUD_REQUIRES_PLOT_HAS_CAMPUS_WITHIN_6' , 'MinRange' ,'0');

	-- RequirementSet and Requirement for City has Pingala National College
	INSERT INTO RequirementSets (RequirementSetId, RequirementSetType)
		VALUES
		('EUD_CITY_HAS_PINGALA_TITLE_4', 'REQUIREMENTSET_TEST_ALL');
	INSERT INTO RequirementSetRequirements (RequirementSetId, RequirementId)
		VALUES
		('EUD_CITY_HAS_PINGALA_TITLE_4', 'EUD_REQUIRES_CITY_HAS_PINGALA_TITLE_4');
	INSERT INTO Requirements (RequirementId, RequirementType)
		VALUES
		('EUD_REQUIRES_CITY_HAS_PINGALA_TITLE_4', 'REQUIREMENT_CITY_HAS_SPECIFIC_GOVERNOR_PROMOTION_TYPE');
	INSERT INTO RequirementArguments (RequirementId, Name, Value)
		VALUES
		('EUD_REQUIRES_CITY_HAS_PINGALA_TITLE_4', 'GovernorPromotionType', 'GOVERNOR_PROMOTION_EUD_EDUCATOR_NATIONAL_COLLEGE');

	INSERT INTO Modifiers (ModifierId , ModifierType, SubjectRequirementSetId)
		VALUES 
		('EUD_NATIONAL_COLLEGE_SCIENCE_ATTACH_REGIONAL_MODIFIER' , 'MODIFIER_PLAYER_CITIES_ATTACH_MODIFIER' , 'EUD_NATIONAL_COLLEGE_SCIENCE_ATTACH_REGIONAL_MODIFIER_REQUIREMENTS'),
		('EUD_NATIONAL_COLLEGE_SCIENCE_SCIENCE_IF_PINGALA_TITLE_4' , 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER' , 'EUD_CITY_HAS_PINGALA_TITLE_4');

	INSERT INTO ModifierArguments (ModifierId , Name , Value)
		VALUES 
		('EUD_NATIONAL_COLLEGE_SCIENCE_ATTACH_REGIONAL_MODIFIER' , 'ModifierId' , 'EUD_NATIONAL_COLLEGE_SCIENCE_SCIENCE_IF_PINGALA_TITLE_4'),
		('EUD_NATIONAL_COLLEGE_SCIENCE_SCIENCE_IF_PINGALA_TITLE_4' , 'YieldType' , 'YIELD_SCIENCE'),
		('EUD_NATIONAL_COLLEGE_SCIENCE_SCIENCE_IF_PINGALA_TITLE_4' , 'Amount' , '10');